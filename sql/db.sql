CREATE DATABASE taller_costura;
USE taller_costura;


CREATE TABLE usuario (
    usuario_id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    contrasena VARCHAR(255) NOT NULL,
    rol VARCHAR(255),
    CI VARCHAR(255),
    fecha_creacion DATE,
    estado TINYINT
);


CREATE TABLE cliente (
    cliente_id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    direccion VARCHAR(255),
    celular VARCHAR(20),
    fecha_creacion DATE,
    estado TINYINT,
    usuario_id INT,
    FOREIGN KEY (usuario_id) REFERENCES usuario(usuario_id)
);


CREATE TABLE tipo_de_trabajo (
    tipo_de_trabajo_id INT PRIMARY KEY AUTO_INCREMENT,
    tipo VARCHAR(50)
);


CREATE TABLE categorias_productos (
    categoria_id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL
);


CREATE TABLE categorias_materiales (
    categoria_id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    unidad VARCHAR(50)
);


CREATE TABLE solicitud (
    solicitud_id INT PRIMARY KEY AUTO_INCREMENT,
    fecha DATE NOT NULL,
    cliente_id INT,
    tipo_de_trabajo_id INT,
    usuario_id INT,
    estado TINYINT,
    FOREIGN KEY (usuario_id) REFERENCES usuario(usuario_id),
    FOREIGN KEY (cliente_id) REFERENCES cliente(cliente_id),
    FOREIGN KEY (tipo_de_trabajo_id) REFERENCES tipo_de_trabajo(tipo_de_trabajo_id)
);


CREATE TABLE detalle_solicitud (
    detalle_solicitud_id INT PRIMARY KEY AUTO_INCREMENT,
    descripcion TEXT,
    cantidad INT,
    solicitud_id INT,
    FOREIGN KEY (solicitud_id) REFERENCES solicitud(solicitud_id)
);


CREATE TABLE personal_requerido (
    personal_requerido_id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(100) NOT NULL,
    habilidades TEXT,
    solicitud_id INT,
    FOREIGN KEY (solicitud_id) REFERENCES solicitud(solicitud_id)
);


CREATE TABLE materiales (
    material_id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(100) NOT NULL,
    cantidad INT,
    color VARCHAR(50),
    precio_compra DECIMAL(10, 2) NOT NULL,
    categoria_id INT,
    fecha_adquisicion DATE,
    descripcion TEXT,
    estado TINYINT,
    FOREIGN KEY (categoria_id) REFERENCES categorias_materiales(categoria_id)
);


CREATE TABLE productos (
    producto_id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(100) NOT NULL,
    precio_unitario DECIMAL(10, 2),
    color VARCHAR(50),
    tamano VARCHAR(20),
    categoria_id INT,
    fecha_lanzamiento DATE,
    descripcion TEXT,
    estado TINYINT,
    FOREIGN KEY (categoria_id) REFERENCES categorias_productos(categoria_id)
);

