const express = require("express")
const mysql = require("mysql")
const bodyParser = require("body-parser")
const crypto = require("crypto");
const session = require("express-session");
const app = express()
require("dotenv").config();


const clave = crypto.randomBytes(32).toString("hex");
app.use(session({
    secret: clave,
    resave: false,
    saveUninitialized: true,
}));


// Crear conexión a MySQL
const connection = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DATABAS,
});

app.use((req, res, next) => {
    res.setHeader('Cache-Control', 'no-store, no-cache, must-revalidate');
    next();
});

function FechaActual() {
    const fechaActual = new Date();
    const anio = fechaActual.getFullYear();
    const mes = String(fechaActual.getMonth() + 1).padStart(2, '0');
    const dia = String(fechaActual.getDate()).padStart(2, '0');
    return `${anio}-${mes}-${dia}`;
}

// const verificarSesion = (req, res, next) => {
//     if (req.session.usuarioId) {
//         next();
//     } else {
//         res.redirect('/');
//     }
// };


connection.connect((err) => {
    if (err) throw err;
    console.log("Conectado a la base de datos MySQL")
});

// Configurar el motor de vistas como EJS
app.set("view engine", "ejs")

// Middleware
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: false }))

app.get('/', function (req, res) {
    const rolUser = "usuario"
    if (req.session.usuarioRol) {
        res.render('inicio.ejs', { usuarioName: req.session.usuarioName, usuarioRol: req.session.usuarioRol });
    } else {
        res.render("inicio.ejs", { usuarioRol: rolUser });
    }
});


app.get('/add_client', (req, res) => {
    let users = "SELECT * FROM usuario"
    connection.query(users, function (error, result_user, fields) {
        if (error) {
            throw error
        }
        res.render('add_client.ejs', { user: result_user });

    })
})

app.get('/materiales', (req, res) => {
    let mat = "SELECT * FROM materiales_view"
    connection.query(mat, function (err, res_mat) {
        if (err) throw err
        let us = "SELECT * FROM usuario"
        connection.query(us, function (err, res_us) {
            if (err) throw err
            res.render('materiales.ejs', {
                materiales: res_mat,
                usuarioRol: req.session.usuarioRol
            })
        })
    })
})

app.get('/productos', (req, res) => {
    let pro = "SELECT * FROM productos_view"
    connection.query(pro, function (err, res_pro) {
        if (err) throw err
        let us = "SELECT * FROM usuario"
        connection.query(us, function (err, res_us) {
            if (err) throw err
            res.render('productos.ejs', {
                productos: res_pro,
                usuarioRol: req.session.usuarioRol
            })
        })
    })
})

app.get("/login", (req, res) => {
    res.render("login.ejs")
})

app.post("/login", (req, res) => {
    const datos = req.body;
    let username = datos.username;
    let password = datos.password;

    console.log(datos);
    let consultar = "SELECT * FROM usuario WHERE nombre = '" + username + "' AND contrasena = '" + password + "'"
    connection.query(consultar, function (error, results) {
        if (error) {
            throw error;
        } else {
            if (results.length == 1) {
                console.log("exito")
                req.session.usuarioId = results[0].usuario_id
                req.session.usuarioName = results[0].nombre
                req.session.usuarioRol = results[0].rol
                res.redirect('/')
            } else {
                console.log("error" + consultar + results.length)
                res.redirect('/login')
            }
        }
    })
})

app.get('/productos', (req, res) => {
    res.render('productos.ejs', { usuarioName: req.session.usuarioName, usuarioRol: req.session.usuarioRol });
});


app.get('/add_user', (req, res) => {
    res.render('add_user.ejs', { usuarioName: req.session.usuarioName, usuarioRol: req.session.usuarioRol });
});

app.post('/add_user', (req, res) => {
    const { nombre, contrasena, rol, ci } = req.body
    const fecha_a = FechaActual()
    const estado = 1
    const insertarUsuario = "INSERT INTO usuario (nombre, contrasena, rol, CI, fecha_creacion, estado) VALUES (?, ?, ?, ?, ?, ?)";
    connection.query(insertarUsuario, [nombre, contrasena, rol, ci, fecha_a, estado], (error, results) => {
        if (error) {
            console.error("Error al agregar el usuario:", error);
            res.redirect('/add_user');
        } else {
            console.log("Usuario agregado correctamente");
            res.redirect('/');
        }
    });
});

app.get('/add_client', (req, res) => {
    res.render('add_client.ejs', { usuarioName: req.session.usuarioName, usuarioRol: req.session.usuarioRol });
});

app.post('/add_client', (req, res) => {
    const estado = 1
    const fecha_a = FechaActual()
    const user_id = 3
    const { nombre, direccion, celular, usuario, password } = req.body;

    let validar = "SELECT * FROM usuario WHERE nombre = '" + usuario + "' AND contrasena = '" + password + "'"
    connection.query(validar, function (err, result) {
        if (err) {
            throw err
        }
        else {
            if (result.length == 1) {
                const user_id = result[0].usuario_id
                const insertarUsuario = "INSERT INTO cliente (nombre, direccion, celular, fecha_creacion, estado, usuario_id) VALUES (?, ?, ?, ?, ?, ?)";
                connection.query(insertarUsuario, [nombre, direccion, celular, fecha_a, estado, user_id], (error, results) => {
                    if (error) {
                        console.error("Error al agregar el cliente:", error);
                        res.redirect('/add_user');
                    } else {
                        console.log("Cliente agregado correctamente");
                        res.redirect('/');
                    }
                });
            }

        }
    })
});

app.get('/add_materiales', (req, res) => {
    let c_mat = "SELECT * FROM categorias_materiales"
    connection.query(c_mat, function (err, c_mat_res){
        if (err) throw err
        res.render("add_materiales.ejs", {materiales : c_mat_res, usuarioRol: req.session.usuarioRol})
    })
});

app.post('/agregar_material', (req, res) => {
    const estado = 1
    const fecha_a = FechaActual()
    const { nombreMaterial, cantidadMaterial, colorMaterial, precioCompra, categoriaMaterial, descripcionMaterial } = req.body;

    const insertarProductoQuery = "INSERT INTO materiales (nombre, cantidad, color, precio_compra, categoria_id, fecha_adquisicion, descripcion, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    connection.query(insertarProductoQuery, [nombreMaterial, cantidadMaterial, colorMaterial, precioCompra, categoriaMaterial, fecha_a, descripcionMaterial, estado], (error, results) => {
        if (error) {
            console.error("Error al agregar el material:", error);
            res.redirect('/add_materiales');
        } else {
            console.log("Material agregado correctamente");
            res.redirect('/add_materiales');
        }
    });
});


app.post('/categorias_materiales', (req, res) => {
    const { nombreCategoria, unidadCategoria } = req.body;

    const insertarCategoriaQuery = "INSERT INTO categorias_materiales (nombre, unidad) VALUES (?, ?)";
    connection.query(insertarCategoriaQuery, [nombreCategoria, unidadCategoria], (error, results) => {
        if (error) {
            console.error("Error al agregar la categoría:", error);
            res.redirect('/add_materiales');
        } else {
            console.log("Categoría agregada correctamente");
            res.redirect('/add_materiales');
        }
    });
});

app.get("/login", (req, res) => {
    res.render("login.ejs")
})

app.get('/add_productos', (req, res) => {
    res.render('add_productos.ejs', { usuarioName: req.session.usuarioName, usuarioRol: req.session.usuarioRol })
})

app.post('/categorias_productos', (req, res) => {
    const { nombreCategoria } = req.body;

    const insertarCategoriaQuery = "INSERT INTO categorias_productos (nombre) VALUES (?)";
    connection.query(insertarCategoriaQuery, [nombreCategoria], (error, results) => {
        if (error) {
            console.error("Error al agregar la categoría:", error);
            res.redirect('/add_productos');
        } else {
            console.log("Categoría agregada correctamente");
            res.redirect('/add_productos');
        }
    });
});

app.get('/servicios', (req, res) => {
    res.render('servicio.ejs');
});

app.get("/logout", (req, res) => {
    req.session.destroy();
    res.redirect('/');
});


// Iniciar el servidor
app.listen(3000, () => {
    console.log("Servidor en ejecución en [1](http://localhost:3000)")
});