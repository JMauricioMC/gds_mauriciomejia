const express = require("express");
const router = express.Router();
const { connection } = require('../connection');
const { fecha } = require("../connection")

router.get('/', (req, res) => {
    let c_mat = "SELECT * FROM categorias_materiales"
    connection.query(c_mat, function (err, c_mat_res){
        if (err) throw err
        res.render("addcatmat.ejs", { usuarioName: req.session.usuarioName, usuarioRol: req.session.usuarioRol })
    })
});

router.post('/', (req, res) => {
    const { nombreCategoria, unidadCategoria } = req.body;

    const insertarCategoriaQuery = "INSERT INTO categorias_materiales (nombre, unidad) VALUES (?, ?)";
    connection.query(insertarCategoriaQuery, [nombreCategoria, unidadCategoria], (error, results) => {
        if (error) {
            console.error("Error al agregar la categoría:", error);
            res.redirect('/');
        } else {
            console.log("Categoría agregada correctamente");
            res.redirect('/');
        }
    });
});

module.exports = router;