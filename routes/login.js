const express = require("express");
const router = express.Router();
const { connection } = require('../connection');

router.get("/", (req, res) => {
    res.render("login.ejs", { usuarioName: req.session.usuarioName, usuarioRol: req.session.usuarioRol })
})

router.post("/", (req, res) => {
    const datos = req.body;
    let username = datos.username;
    let password = datos.password;

    let consultar = "SELECT * FROM usuario WHERE nombre = ? AND contrasena = ?";
    connection.query(consultar, [username, password], function (error, results) {
        if (error) {
            console.error(error);
            return res.render('login.ejs', { error: 'Ocurrió un error, por favor intenta de nuevo más tarde.' });
        } else {
            if (results.length == 1) {
                console.log("exito");
                req.session.usuarioId = results[0].usuario_id;
                req.session.usuarioName = results[0].nombre;
                req.session.usuarioRol = results[0].rol;
                res.redirect("/");
            } else {
                res.render("login.ejs", { error: "Usuario o contraseña incorrectos" });
            }
        }
    });
});

module.exports = router;