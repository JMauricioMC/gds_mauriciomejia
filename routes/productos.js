const express = require("express");
const router = express.Router();
const { connection } = require('../connection');
const { fecha } = require("../connection")

router.get('/', (req, res) => {
    let pro = "SELECT * FROM productos_view"
    connection.query(pro, function (err, res_pro) {
        if (err) throw err
        let us = "SELECT * FROM usuario"
        connection.query(us, function (err, res_us) {
            if (err) throw err
            res.render('productos.ejs', {
                productos: res_pro,
                usuarioRol: req.session.usuarioRol
            })
        })
    })
})

// router.get('/productos', (req, res) => {
//     res.render('productos.ejs', { usuarioName: req.session.usuarioName, usuarioRol: req.session.usuarioRol });
// });

module.exports = router;