const express = require("express");
const router = express.Router();
const { connection } = require('../connection');
const { fecha } = require("../connection")

router.get('/', (req, res) => {
    res.render('servicio.ejs', { usuarioName: req.session.usuarioName, usuarioRol: req.session.usuarioRol });
});

module.exports = router;