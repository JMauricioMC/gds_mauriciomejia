const express = require("express");
const router = express.Router();
const { connection } = require('../connection');
const { fecha } = require("../connection")

router.get('/', (req, res) => {
    res.render('addcatpro.ejs', { usuarioName: req.session.usuarioName, usuarioRol: req.session.usuarioRol })
})

router.post('/', (req, res) => {
    const { nombreCategoria } = req.body;

    const insertarCategoriaQuery = "INSERT INTO categorias_productos (nombre) VALUES (?)";
    connection.query(insertarCategoriaQuery, [nombreCategoria], (error, results) => {
        if (error) {
            console.error("Error al agregar la categoría:", error);
            res.redirect('/');
        } else {
            console.log("Categoría agregada correctamente");
            res.redirect('/');
        }
    });
});

module.exports = router;