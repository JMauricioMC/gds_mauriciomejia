const express = require("express");
const router = express.Router();
const { connection } = require('../connection');
const { fecha } = require("../connection")

router.get('/', (req, res) => {
    let mat = "SELECT * FROM materiales_view"
    connection.query(mat, function (err, res_mat) {
        if (err) throw err
        let us = "SELECT * FROM usuario"
        connection.query(us, function (err, res_us) {
            if (err) throw err
            res.render('materiales.ejs', {
                materiales: res_mat,
                usuarioRol: req.session.usuarioRol
            })
        })
    })
})

module.exports = router;