const express = require("express");
const router = express.Router();
const { connection } = require('../connection');
const { fecha } = require("../connection")


router.get('/', (req, res) => {
    res.render('user.ejs', { usuarioName: req.session.usuarioName, usuarioRol: req.session.usuarioRol });
});


router.post("/", (req, res) => {
    const { nombre, contrasena, rol, ci } = req.body
    const estado = 1
    const insertarUsuario = "INSERT INTO usuario (nombre, contrasena, rol, CI, fecha_creacion, estado) VALUES (?, ?, ?, ?, ?, ?)";
    connection.query(insertarUsuario, [nombre, contrasena, rol, ci, fecha, estado], (error, results) => {
        if (error) {
            console.error("Error al agregar el usuario:", error);
            res.redirect('/user');
        } else {
            console.log("Usuario agregado correctamente");
            res.redirect('/');
        }
    });
});

module.exports = router;