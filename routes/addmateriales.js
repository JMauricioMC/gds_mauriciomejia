const express = require("express");
const router = express.Router();
const { connection } = require('../connection');
const { fecha } = require("../connection")

router.get('/', (req, res) => {
    let c_mat = "SELECT * FROM categorias_materiales"
    connection.query(c_mat, function (err, c_mat_res){
        if (err) throw err
        res.render("add_materiales.ejs", {materiales : c_mat_res, usuarioRol: req.session.usuarioRol})
    })
});

router.post('/', (req, res) => {
    const estado = 1
    const { nombreMaterial, cantidadMaterial, colorMaterial, precioCompra, categoriaMaterial, descripcionMaterial } = req.body;

    const insertarProductoQuery = "INSERT INTO materiales (nombre, cantidad, color, precio_compra, categoria_id, fecha_adquisicion, descripcion, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    connection.query(insertarProductoQuery, [nombreMaterial, cantidadMaterial, colorMaterial, precioCompra, categoriaMaterial, fecha, descripcionMaterial, estado], (error, results) => {
        if (error) {
            console.error("Error al agregar el material:", error);
            res.redirect('/');
        } else {
            console.log("Material agregado correctamente");
            res.redirect('/');
        }
    });
});

module.exports = router;