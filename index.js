const express = require("express")
const mysql = require("mysql")
const bodyParser = require("body-parser")
const crypto = require("crypto");
const session = require("express-session");
const app = express()
require("dotenv").config();

const { fecha } = require("./connection")
const { connection } = require("./connection")
const loginRoute = require("./routes/login")
const userRoute = require("./routes/user")
const clientRoute = require("./routes/client")
const materialRoute = require("./routes/materiales")
const productRoute = require("./routes/productos")
const servRoute = require("./routes/servicio")
const addcpRoute = require("./routes/addcatpro")
const addmRoute = require("./routes/addmateriales")
const addcmRoute = require("./routes/addcatmat")
const servicesRoute = require("./routes/servicio")
const addproute = require("./routes/addproductos")

const clave = crypto.randomBytes(32).toString("hex");
app.use(session({
    secret: clave,
    resave: false,
    saveUninitialized: true,
}));

app.use((req, res, next) => {
    res.setHeader('Cache-Control', 'no-store, no-cache, must-revalidate');
    next();
});


// Configurar el motor de vistas como EJS
app.set("view engine", "ejs")

// Middleware
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: false }))

app.get('/', function (req, res) {
    const rolUser = "usuario"
    if (req.session.usuarioRol) {
        res.render('inicio.ejs', { usuarioName: req.session.usuarioName, usuarioRol: req.session.usuarioRol });
    } else {
        res.render("inicio.ejs", { usuarioRol: rolUser });
    }
});


app.use("/login", loginRoute)

app.use("/user", userRoute)

app.use("/client", clientRoute)

app.use("/materiales", materialRoute)

app.use("/productos", productRoute)

app.use("/servicios", servRoute)

app.use("/add_materiales", addmRoute)

app.use("/addcatmaterial", addcmRoute)

app.use("/addcatproduct", addcpRoute)

app.use("/services", servicesRoute)

app.use("/add_product", addproute)




app.get("/logout", (req, res) => {
    req.session.destroy();
    res.redirect('/');
});

app.listen(3000, () => {
    console.log("Servidor en ejecución en [1](http://localhost:3000)")
});